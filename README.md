#PHP recruitment challenge - codetain
##About the application

This application was made as part of the recruitment process for the codetain company. This application is used to create an auction. 
A registered user can manage his products. After entering the home page, we see a list of all auctions. Auctions can be filtered by assigned 
category and sorted by date of creation and price. In the application it is possible to change the currency between PLN and EUR. At 24:00,
 the application downloads data on the exchange rate and saves it to the database.

##Installation

To install the application, we need to download the repository and execute the following commands in the 
application directory. We create a file with our configuration and download the necessary dependencies.
With the last command, we generate the application key.

```bash
cp .env.example .env
composer install
npm intall
npm run prod
php artisan key:generate
```

Before following these steps, we need to configure the database connection in our .env configuration file.
These commands create the structure of the database and fill it with the available categories.
The last application creates a symbolic link so that the photos display correctly.
```bash
php artisan migrate
php artisan init
php artisan storage:link
```

To perform tasks at a given time, we can configure the cron table by adding the following task.
```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

The End :)