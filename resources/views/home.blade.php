@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        Categories
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-pills  flex-column">
                            @foreach($categories as $category)
                                <li class="nav-item">
                                    <a class="nav-link  @if($category->name === $activeCategory) active @endif" href="{{ route('home', ['category' => $category->name]) }}">{{ $category->name }}</a>
                                </li>
                            @endforeach
                                <a class="btn-primary btn mt-1" href=" {{ route('home') }}">Clear</a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-10">{{ $products->links() }}</div>
                    <div class="col-md-2">
                        <div class="dropdown float-right">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                                Sort by
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($sort as $s)
                                    <li><a class="dropdown-item" href="{{ url()->current() . '?sort=' . $s['searchName']}}">{{ $s['name'] }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                @if(count($products) > 0)
                @foreach($products as $product)
                    <div class="card mt-1">
                        <a class="text-decoration-none product-link" href="{{ route('products.show', $product->id) }}"><div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        @if($product->image)
                                            <img class="img-fluid" src="{{ asset($product->image->path) }}" alt="">
                                        @else
                                            <img class="img-fluid" src="{{ asset('images/noimage.png') }}" alt="">
                                        @endif
                                    </div>
                                    <div class="col-md-9 mt-4">
                                        <div><span class="font-weight-bold">Price(netto): </span><span>{{ $product->price_netto . " " . $currency }}</span></div>
                                        <div><span class="font-weight-bold">Price(brutto): </span><span>{{ $product->price_brutto . " " . $currency }}</span></div>
                                        <div><span class="font-weight-bold">Name: </span><span>{{ $product->name }}</span></div>
                                    </div>
                                </div>
                            </div></a>

                    </div>
                @endforeach
                @else
                    <h2>Products not found</h2>
                @endif
            </div>
        </div>
    </div>
@endsection
