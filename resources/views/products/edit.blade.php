@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit product</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('products.update', $product->id) }} " enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>
                                <div class="col-md-9">
                                    <input id="name" type="text" placeholder="Your product name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-2 col-form-label text-md-right">Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Your product description"  id="description" rows="6" required autocomplete="description" autofocus>{{ $product->description }}</textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-md-2 col-form-label text-md-right">Category</label>
                                <div class="col-md-9">
                                    <select class="form-select @error('category') is-invalid @enderror" name="category" id="category">
                                        <option value="0">Choose category</option>
                                        @foreach ($categories as $category)
                                            <option @if($category->name === $product->category->name) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-2 col-form-label text-md-right">Price(netto)</label>
                                <div class="col-md-9">
                                    <div class="input-group mb-3">
                                        <input id="price" placeholder="Product price" step="any" type="number" min="0" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $product->price_netto }}" required autocomplete="price" autofocus>
                                        <span class="input-group-text" id="basic-addon2">{{ $currency }}</span>
                                    </div>

                                    @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-md-2 col-form-label text-md-right">Image</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="image" type="file" id="image" accept="image/png, image/jpeg">

                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-9 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit product') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
