@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h2>{{ $product->name }}</h2></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                @if($product->image)
                                    <img class="img-fluid" src="{{ asset($product->image->path) }}" alt="">
                                @else
                                    <img class="img-fluid" src="{{ asset('images/noimage.png') }}" alt="">
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h3>Parameters</h3>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                        <tr>
                                            <td class="font-weight-bold w-25">Category</td>
                                            <td class="w-75">{{ $product->category->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold w-25">Description</td>
                                            <td class="w-75" style="white-space: pre-wrap;">{{ $product->description }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold w-25">Price(netto)</td>
                                            <td class="w-75">{{ $product->price_netto . " " . $currency}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold  w-25">Price(brutto)</td>
                                            <td class="w-75">{{ $product->price_brutto . " " . $currency}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
