@extends('layouts.app')



@section('content')

<div class="container">

    @if ($message = \Illuminate\Support\Facades\Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

        @if ($message = \Illuminate\Support\Facades\Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Products') }}
                    <a class="nav-link float-right" href="{{ route('products.create') }}">Add Product</a>
                </div>

                <div class="card-body">
                    @if(count($products) !== 0)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Price(netto)</th>
                                <th scope="col">Price(brutto)</th>
                                <th scope="col">Category</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price_netto . " " . $currency }}</td>
                                        <td>{{ $product->price_brutto . " " . $currency }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>
                                            <form action="{{ route('products.destroy',$product->id) }}" method="POST">

                                                <a class="btn btn-primary" href="{{ route('products.show',$product->id) }}">Show</a>

                                                <a class="btn btn-warning" href="{{ route('products.edit',$product->id) }}">Edit</a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    @else

                        <h2>You dont have products</h2>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



