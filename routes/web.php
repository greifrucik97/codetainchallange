<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('setCurrency', 'SessionController@setCurrency')->name('setCurrency');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/{category?}', 'HomeController@index')->name('home');

Route::GET('products/{product}', 'ProductController@show')->name('products.show');

Route::GET('dashboard/products', 'ProductController@index')->name('products.index');
Route::POST('dashboard/products', 'ProductController@store')->name('products.store');
Route::GET('dashboard/products/create', 'ProductController@create')->name('products.create');
Route::PATCH('dashboard/products/{products}', 'ProductController@update')->name('products.update');
Route::DELETE('/dashboard/products/{product}', 'ProductController@destroy')->name('products.destroy');
Route::GET('/dashboard/products/{product}/edit', 'ProductController@edit')->name('products.edit');


