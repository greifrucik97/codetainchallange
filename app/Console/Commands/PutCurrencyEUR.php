<?php

namespace App\Console\Commands;

use App\Http\Helpers\CurrencyHelper;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;

/**
 * Insert euro exchange to database
 * Class PutCurrencyEUR
 * @package App\Console\Commands
 */
class PutCurrencyEUR extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:eur';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     * @throws GuzzleException
     */
    public function handle()
    {
        $currencyEUR = CurrencyHelper::getTodayCurrencyEUROrNull();

        if ($currencyEUR) {
            $currencyEUR->save();
        }

        return 0;
    }
}
