<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Init system
 * Class InitApplication
 * @package App\Console\Commands
 */
class InitApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command initApplication';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->call('command:initCategories');
        return 0;
    }
}
