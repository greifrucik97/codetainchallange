<?php

namespace App\Console\Commands;

use App\Category;
use Illuminate\Console\Command;

/**
 * Insert categories to database
 * Class PutCategories
 * @package App\Console\Commands
 */
class PutCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:initCategories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = [
            'Automotive',
            'Electronics',
            'House and Garden',
            'Sport'
        ];
        foreach ($categories as $category) {
            $cat = Category::where('name', $category)->first();
            if (!$cat) {
                $c = new Category();
                $c->name = $category;
                $c->save();
            }
        }
        return 0;
    }
}
