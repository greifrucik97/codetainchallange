<?php

declare(strict_types = 1);

namespace App\Http\Interfaces;

use App\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface ProductServiceInterface
 * @package App\Http\Interfaces
 */
interface ProductServiceInterface
{
    /**
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $category
     * @param $image
     * @return bool
     */
    public function storeProduct(string $name, string $description, float $price, int $category, $image): bool;

    /**
     * @param string $currency
     * @return array
     */
    public function getUserProducts(string $currency): array;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteProduct(int $id): bool;

    /**
     * @param int $id
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $category
     * @param $imageRequest
     * @return bool
     */
    public function updateProduct(int $id, string $name, string $description, float $price, int $category, $imageRequest): bool;

    /**
     * @param string|null $category
     * @param string|null $sort
     * @return LengthAwarePaginator
     */
    public function getProductsWithSort(?string $category, ?string $sort): LengthAwarePaginator;

    /**
     * @param string $currency
     * @param int $id
     * @return Product
     */
    public function getProduct(string $currency, int $id): Product;
}