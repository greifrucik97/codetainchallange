<?php

declare(strict_types = 1);

namespace App\Http\Constant;

/**
 * Class PaginationConstant
 * @package App\Http\Constant
 */
class PaginationConstant
{
    public const PER_PAGE_PRODUCTS = 3;
}