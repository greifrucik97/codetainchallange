<?php

declare(strict_types = 1);

namespace App\Http\Constant;

/**
 * Class SortConstant
 * @package App\Http\Constant
 */
class SortConstant
{
    public const SORT_LIST = [
        [
            'name' => 'Date added descending',
            'searchName' => 'date_desc'
        ],
        [
            'name' => 'Date added ascending',
            'searchName' => 'date_asc'
        ],
        [
            'name' => 'Price descending',
            'searchName' => 'price_desc'
        ],
        [
            'name' => 'Price ascending',
            'searchName' => 'price_asc'
        ],
    ];
}