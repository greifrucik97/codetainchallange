<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Helpers\SessionHelper;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class SessionController
 * @package App\Http\Controllers
 */
class SessionController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function setCurrency(Request $request): RedirectResponse
    {
        SessionHelper::updateCurrency($request->get('currency'));

        return redirect()->back();
    }
}
