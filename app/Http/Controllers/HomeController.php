<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Category;
use App\Http\Constant\SortConstant;
use App\Http\Helpers\CurrencyHelper;
use App\Http\Helpers\SessionHelper;
use App\Http\Services\ProductService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * @var ProductService
     */
    private $service;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->service = new ProductService();
    }

    /**
     * Render home page
     * @param null $category
     * @return View
     * @throws GuzzleException
     */
    public function index($category = null): View
    {
        $sort = request()->get('sort');

        $products = $this->service->getProductsWithSort($category, $sort);

        $currency = SessionHelper::getUserCurrency();

        $productsItems = CurrencyHelper::getProductsInCurrency($products->items(), $currency);

        $products->item = $productsItems;

        return view('home', [
            'products' => $products,
            'categories' => Category::all(),
            'activeCategory' => $category,
            'sort' => SortConstant::SORT_LIST,
            'currency' => $currency
        ]);
    }
}
