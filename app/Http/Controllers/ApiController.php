<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Helpers\CurrencyHelper;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{

    /**
     * Get euro exchange rate to api
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getEuro(): JsonResponse
    {
        return response()->json(CurrencyHelper::getTodayCurrencyEUROrLastInDatabase());
    }
}
