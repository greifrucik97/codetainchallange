<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Category;
use App\Http\Helpers\SessionHelper;
use App\Http\Services\ProductService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    private $service;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->service = new ProductService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws GuzzleException
     */
    public function index(): View
    {
        $currency = SessionHelper::getUserCurrency();

        $products = $this->service->getUserProducts($currency);

        return view('products.index', [
            'products' => $products,
            'currency' => $currency
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $categories = Category::all();
        $currency = SessionHelper::getUserCurrency();

        return view('products.create', [
            'categories' => $categories,
            'currency' => $currency
        ]);
    }

    /**
     * Store product
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'category' => 'required|exists:categories,id',
            'price' => 'required|numeric',
            'image' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        ]);

        $price = $request->get('price');

        $created = $this->service->storeProduct($request->get('name'), $request->get('description'), (float)$price, (int)$request->get('category'), $request->image);

        if ($created) {
            return redirect()->route('products.index')->with('success', 'The product has been added');
        } else {
            return redirect()->route('products.index')->with('error', 'The product has not been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     * @throws GuzzleException
     */
    public function show($id): View
    {
        $currency = SessionHelper::getUserCurrency();

        $product = $this->service->getProduct($currency, (int)$id);

        return view('products.show', [
            'product' => $product,
            'currency' => $currency
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     * @throws GuzzleException
     */
    public function edit($id): View
    {
        $currency = SessionHelper::getUserCurrency();

        $product = $this->service->getProduct($currency, (int)$id);

        return view('products.edit', [
            'product' => $product,
            'categories' => Category::all(),
            'currency' => $currency
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'category' => 'numeric|required|exists:categories,id',
            'price' => 'required|numeric',
            'image' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        ]);

        $price = $request->get('price');

        $updated = $this->service->updateProduct((int)$id, $request->get('name'), $request->get('description'), (float)$price, (int)$request->get('category'), $request->image);

        if ($updated) {
            return redirect()->route('products.index')->with('success', 'The product has been updated');
        } else {
            return redirect()->route('products.index')->with('error', 'The product has not been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $deleted = $this->service->deleteProduct((int)$id);

        if ($deleted) {
            return redirect()->route('products.index')->with('success', 'The product has been removed');
        } else {
            return redirect()->route('products.index')->with('error', 'You can remove the product on Monday from 12:00 to 12:04');
        }
    }
}
