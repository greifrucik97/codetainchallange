<?php

declare(strict_types = 1);

namespace App\Http\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class RequestHelper
 * @package App\Http\Helpers
 */
class RequestHelper
{
    //Addresses
    public const ADDRESS_GET_EURO_TODAY = 'http://api.nbp.pl/api/exchangerates/rates/c/eur/today?format=json';
    public const ADDRESS_GET_EURO_AVG_TODAY = 'http://api.nbp.pl/api/exchangerates/rates/a/eur?format=json';

    /**
     * @param string $address
     * @return mixed|null
     * @throws GuzzleException
     */
    public static function getRequest(string $address){
        $client = new Client();
        $res = $client->get($address);
        if  ($res->getStatusCode() === 200) {
            $data = json_decode($res->getBody()->getContents(), true);
            return $data;
        } else {
            return null;
        }
    }
}