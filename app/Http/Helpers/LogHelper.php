<?php


namespace App\Http\Helpers;

use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class LogHelper
 * @package App\Http\Helpers
 */
class LogHelper
{
    /**
     * @param string $productName
     */
    public static function logInfoRemoveProduct(string $productName): void
    {
        $user = Auth::user();
        Log::channel('removeProducts')->info($user->name . '('. $user->id .')' . " removed the product named " . $productName);
    }

    /**
     * @param Product $product
     */
    public static function logErrorRemoveProduct(Product $product): void
    {
        $user = Auth::user();
        Log::channel('removeProducts')->info($user->name . '('. $user->id .')' . " could not remove the product named " . $product->name . '(' . $product->id . ')');
    }

}