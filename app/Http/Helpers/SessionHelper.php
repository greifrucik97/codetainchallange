<?php

declare(strict_types = 1);

namespace App\Http\Helpers;

/**
 * Class SessionHelper
 * @package App\Http\Helpers
 */
class SessionHelper
{
    /**
     * Get session currency
     *
     * @return string
     */
    public static function getUserCurrency(): string
    {
        return (session()->has('currency')) ? session()->get('currency') : "PLN";
    }

    /**
     * Update session currency
     *
     * @param string $currency
     */
    public static function updateCurrency(string $currency): void
    {
        if (session()->has('currency')) {
            $sessionCurrency = session()->get('currency');
            if ($sessionCurrency !== $currency) {
                session()->put('currency', $currency);
            }
        } else {
            session()->put('currency', $currency);
        }
    }

}