<?php

declare(strict_types = 1);

namespace App\Http\Helpers;

use App\CurrencyEUR;
use App\Product;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class CurrencyHelper
 * @package App\Http\Helpers
 */
class CurrencyHelper
{

    /**
     * Get today euro exchange or null
     * @return CurrencyEUR|null
     * @throws GuzzleException
     */
    public static function getTodayCurrencyEUROrNull(): ?CurrencyEUR
    {
        $data = RequestHelper::getRequest(RequestHelper::ADDRESS_GET_EURO_TODAY);
        if ($data) {
            $currencyEUR = new CurrencyEUR();
            $currencyEUR->effectiveDate = $data['rates'][0]['effectiveDate'];
            $currencyEUR->bid = $data['rates'][0]['bid'];
            $currencyEUR->ask = $data['rates'][0]['ask'];
            return $currencyEUR;
        }
        return null;
    }

    /**
     * Get today euro exchange or last in database
     * @return CurrencyEUR
     * @throws GuzzleException
     */
    public static function getTodayCurrencyEUROrLastInDatabase(): CurrencyEUR
    {
        $currencyEUR = self::getTodayCurrencyEUROrNull();
        if (!$currencyEUR) {
            $currencyEUR = CurrencyEUR::orderBy('created_at', 'desc')->first();
        }
        return $currencyEUR;
    }

    /**
     * Get avg euro exchange
     * @return float
     * @throws GuzzleException
     */
    public static function getAvgCurrencyEURO(): float
    {
        $data = RequestHelper::getRequest(RequestHelper::ADDRESS_GET_EURO_AVG_TODAY);

        return $data['rates'][0]['mid'];
    }

    /**
     * Get product in specific currency
     * @param Product $product
     * @param string $currency
     * @param float $eurExchange
     * @return Product
     */
    public static function getProductInCurrency(Product $product, string $currency, float $eurExchange): Product
    {
        if ($currency === 'EUR' && $product->currency === "PLN") {
            $product->price_netto = number_format((float)$product->price_netto / $eurExchange, 2, '.', '');
            $product->price_brutto = number_format((float)$product->price_brutto / $eurExchange, 2, '.', '');
        } else if ($currency === 'PLN' && $product->currency === "EUR") {
            $product->price_netto = number_format((float)$product->price_netto * $eurExchange, 2, '.', '');
            $product->price_brutto = number_format((float)$product->price_brutto * $eurExchange, 2, '.', '');
        } else {
            $product->price_netto = number_format((float)$product->price_netto, 2, '.', '');
            $product->price_brutto = number_format((float)$product->price_brutto, 2, '.', '');
        }

        return $product;
    }

    /**
     * Get products in specific currency
     * @param $products
     * @param string $currency
     * @return array
     * @throws GuzzleException
     */
    public static function getProductsInCurrency($products, string $currency): array
    {
        $eurExchange = self::getAvgCurrencyEURO();
        $arrayOfNewProducts = [];
        foreach ($products as $product) {
            $newProduct = self::getProductInCurrency($product, $currency, $eurExchange);
            array_push($arrayOfNewProducts, $newProduct);
        }
        return $arrayOfNewProducts;
    }


}