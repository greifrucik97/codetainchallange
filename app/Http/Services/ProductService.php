<?php

declare(strict_types = 1);

namespace App\Http\Services;

use App\Category;
use App\Http\Constant\PaginationConstant;
use App\Http\Helpers\CurrencyHelper;
use App\Http\Helpers\LogHelper;
use App\Http\Helpers\SessionHelper;
use App\Http\Interfaces\ProductServiceInterface;
use App\Image;
use App\Product;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Class ProductService
 * @package App\Http\Services
 */
class ProductService implements ProductServiceInterface
{

    /**
     * Store product
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $category
     * @param $imageRequest
     * @return bool
     */
    public function storeProduct(string $name, string $description, float $price, int $category, $imageRequest): bool
    {
        if (Auth::check()) {
            $product = new Product();
            $product->name = $name;
            $product->description = $description;
            $product->price_netto = $price;
            $product->price_brutto = $price * 1.23;
            $product->category_id = $category;
            $product->user_id = Auth::id();
            $product->currency = SessionHelper::getUserCurrency();

            $product->save();

            $this->createImage($imageRequest, $product);

            return true;
        }
        return false;
    }

    /**
     * Delete products in specific time
     * @param int $id
     * @return bool
     */
    public function deleteProduct(int $id): bool
    {
        $product = Product::find($id);

        if ($product->user_id === Auth::id()) {
            $time = Carbon::now();
            $dayOfWeek = $time->dayOfWeek;
            $hour = $time->hour;
            $minute = $time->minute;
            if ($dayOfWeek === 1 && $hour === 12 && $minute >= 0 && $minute < 5) {
                $image = $product->image;
                if ($image) {
                    Storage::disk('public')->delete($image->path);
                    $image->delete();
                }
                $productName = $product->name;
                $product->delete();
                LogHelper::logInfoRemoveProduct($productName);
                return true;
            }
            LogHelper::logErrorRemoveProduct($product);
            return false;
        }
        LogHelper::logErrorRemoveProduct($product);
        return false;
    }

    /**
     * Update product
     * @param int $id
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $category
     * @param $imageRequest
     * @return bool
     */
    public function updateProduct(int $id, string $name, string $description, float $price, int $category, $imageRequest): bool
    {
        $product = Product::find($id);
        if ($product->user_id === Auth::id()) {
            $product->name = $name;
            $product->description = $description;
            $product->price_netto = $price;
            $product->category_id = $category;
            $product->price_netto = $price;
            $product->price_brutto = $price * 1.23;
            $product->currency = SessionHelper::getUserCurrency();
            $product->save();

            $this->createImage($imageRequest,$product);

            return true;
        }
        return false;
    }

    /**
     * Create image
     * @param $imageRequest
     * @param Product $product
     */
    private function createImage($imageRequest, Product $product): void
    {
        if ($imageRequest) {
            $uniqueName = time() . Str::random(5). "." . $imageRequest->getClientOriginalExtension();
            $path = Storage::disk('public')->putFileAs('images', new File($imageRequest->path()), $uniqueName);
            $image = new Image();
            $image->product_id = $product->id;
            $image->path = Storage::url($path);
            $image->save();
        }
    }

    /**
     * Get products with sort elements and specific category
     *
     * @param string|null $category
     * @param string|null $sort
     * @return LengthAwarePaginator
     */
    public function getProductsWithSort(?string $category, ?string $sort): LengthAwarePaginator
    {
        if ($category !== null) {
            $cat = Category::where('name', $category)->first();
            if ($sort) {
                $s = explode('_', $sort);
                if ($s[0] === 'date') {
                    return Product::where('category_id', $cat->id)->orderBy('created_at', $s[1])->with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS)->appends('sort', $sort);
                } else if ($s[0] === 'price') {
                    return Product::where('category_id', $cat->id)->orderBy('price_netto', $s[1])->with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS)->appends('sort', $sort);
                }
            }

            return Product::where('category_id', $cat->id)->with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS);
        } else {
            if ($sort) {
                $s = explode('_', $sort);
                if ($s[0] === 'date') {
                    return Product::orderBy('created_at', $s[1])->with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS)->appends('sort', $sort);
                } else if ($s[0] === 'price') {
                    return Product::orderBy('price_netto', $s[1])->with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS)->appends('sort', $sort);
                }
            }

            return Product::with(['image', 'user', 'category'])->paginate(PaginationConstant::PER_PAGE_PRODUCTS);
        }
    }

    /**
     * Get user products in specific currency
     *
     * @param string $currency
     * @return array
     * @throws GuzzleException
     */
    public function getUserProducts(string $currency): array
    {
        $user = Auth::user();
        return CurrencyHelper::getProductsInCurrency($user->products()->with(['category'])->get(), $currency);
    }

    /**
     * Get product in specific currency
     *
     * @param string $currency
     * @param int $id
     * @return Product
     * @throws GuzzleException
     */
    public function getProduct(string $currency, int $id): Product
    {
        $eurExchange = CurrencyHelper::getAvgCurrencyEURO();
        return CurrencyHelper::getProductInCurrency(Product::where('id', $id)->with(['image', 'category', 'user'])->first(), $currency, $eurExchange);
    }

}